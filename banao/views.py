from django.shortcuts import render

from django.http import HttpResponse

def index(request):
    return render(request, 'welcome.html')


# def default_page(request):
#     return render(request, 'welcome.html')