from django.urls import path
from banao import views

urlpatterns = [
    path('', views.index, name='index'),
]
