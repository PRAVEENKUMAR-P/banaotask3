from django.apps import AppConfig


class BanaoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'banao'
