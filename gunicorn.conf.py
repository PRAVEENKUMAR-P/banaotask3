from __future__ import unicode_literals
import multiprocessing

chdir = "/var/www/banaoTask3/"
pythonpath = "/var/www/banaoTask3/"
user = "www-data"
group = "www-data"

bind = "127.0.0.1:8001"
workers = multiprocessing.cpu_count() + 1
loglevel = "debug"
proc_name = "banao"
timeout = 7200
keepalive = 7200
errorlog = "/var/log/banao-error.log"
accesslog = "/var/log/banao-access.log"
